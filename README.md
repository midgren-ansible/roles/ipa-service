# Role ipa_service

This is a helper role to easily create Kerberos services on a host.

It uses the default inventory variables for parameters that are required to
access the IPA server, and is thus handier to use than the module
`community.general.ipa_service` directly (which this role in turn uses
specifying those default variables.)

Default values used:
* The IPA server to connect to: The first entry of `ipa_client_servers`
* The user with administrator privileges to connect as: `ipa_admin_user`
  (or 'admin' if ipa_admin_user is not set).
* Password for the specified user: `ipaadmin_password`

## Variables

* `ipa_service_principals`

    List of principals to create. A list of strings with the unique part of the
    principle only, i.e. excluding the FQDN. A principal may look like this:
    ```
    http/nextcloud.internal.midgren.net
    ```
    To create this principal, `ipa_service_principals` would simply contain ['http'].
